# Radio

## LoRa module

For this project we use LoRa Ra-02 SX1278 (433 MHz). It communicates with ESP32 Devkit V1 by SPI protocol. LoRa is connected to 
antenna with U.FL/IPEX UMC 2mm. Module is powerd by 3.3 voltage from ESP32.


## TinyGS - configuration

After uploading the firmware to ESP32 from website https://github.com/G4lile0/tinyGS, we had to configure to connect 
to the network.

